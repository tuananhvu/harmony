import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
import detectEthereumProvider from '@metamask/detect-provider';

function App() {

    const [walletAddress, setWalletAddress] = useState("");
    let ethAddress;
    let isAuthorised = false;
    if (typeof window.ethereum !== 'undefined') {
        console.log('MetaMask is installed!');
    }


  const handleAccountsChanged = (accounts) => {
    if (accounts.length === 0) {
      console.error('Not found accounts');
    } else {
      ethAddress = accounts[0];
      setWalletAddress(ethAddress);
    }
  };
const send = async () => {
    const transactionParameters = {
        nonce: '0x00', // ignored by MetaMask
        gasPrice: '0x09184e72a000', // customizable by user during MetaMask confirmation.
        gas: '21000', // customizable by user during MetaMask confirmation.
        to: '0x2076A228E6eB670fd1C604DE574d555476520DB7', // Required except during contract publications.
        from: window.ethereum.selectedAddress, // must match user's active address.
        value: '1', // Only required to send ether to the recipient from the initiating external account.
        chainId: 1666600000, // Used to prevent transaction reuse across blockchains. Auto-filled by MetaMask.
    };

// txHash is a hex string
// As with any RPC call, it may throw an error
    const txHash = await window.ethereum.request({
        method: 'eth_sendTransaction',
        params: [transactionParameters],
    });
    console.log('txHash', txHash)
}
  const signInMetamask = async () => {
    const provider = await detectEthereumProvider();

    // @ts-ignore
    if (provider !== window.ethereum) {
      console.error('Do you have multiple wallets installed?');
    }

    if (!provider) {
      console.error('Metamask not found');
      return;
    }

    // MetaMask events
    provider.on('accountsChanged', handleAccountsChanged);

    provider.on('disconnect', () => {
      console.log('disconnect');
      isAuthorised = false;
    });

    provider.on('chainIdChanged', chainId => console.log('chainIdChanged', chainId));

    provider
        .request({ method: 'eth_requestAccounts' })
        .then(async params => {
          handleAccountsChanged(params);
          isAuthorised = true;
        })
        .catch(err => {
          isAuthorised = false;

          if (err.code === 4001) {
            console.error('Please connect to MetaMask.');
          } else {
            console.error(err);
          }
        });
  };
  return (
      <div className="App">
        <header className="App-header">
          <button onClick={signInMetamask}>Request Account</button>
          <button onClick={send}>send</button>
          <h3>Wallet Address: {walletAddress}</h3>
        </header>
      </div>
  );
}

export default App;
